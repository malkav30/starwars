# License Categories

.center.image100[![alt text](img/grogu.webp)]

---
# License Categories 

.pull-left[
  .center.image100[![alt text](img/leia.webp)]
]

.pull-right[
  .center.image100[![alt text](img/mandalorian.webp)]
]

---
# open Source Licences

**Two main entity validate Licenses**

.pull-left[
  .center[Open Source Initiative]
  .center.image40[![alt text](img/OSI.png)]
]

.pull-right[
  .center[Free Software Foundation]
  .center.image40[![alt text](img/FSF.png)]
]

---
# Definitions

Key points:

- **Copyright**: Legal rights to control an original **creative work**, granted by each country’s laws

- **Patents**: Legal rights to control an **invention**, granted by each country’s laws

- **Trademarks**: Legal rights to control a **mark** — a name or logo designating the origin of goods or services


---

# Example: mit

Very small License (15 lines)

Key criterions:
.pull-left[
- **Linking**: Permissive (when the code is provided as a library)
- **Distribution**: Permissive
- **Modification**: Permissive
- **Private Use**: Yes
- **Patent Grant**: Manual
- **Sublicensing**: Permissive
- **TM grant**: Manually
]

.pull-right[.center.image40[![MIT](img/MIT.png)]]

---
# open Source Licenses

.pull-left[
There are more than 100 factions (licenses)

We choose to split licenses in 3 categories:

- **Permissive**: If you redistribute the software, also provide its license and copyright notices.

- **Strong Copyleft**: If you redistribute the software, also provide the same freedoms / rights to downstream recipients (Even if the code is used as a **library**)

- **Weak Copyleft**: Similar to Strong Copyleft, with differences in the boundaries for the software to which the copyleft obligations apply
]

.pull-right[.center.image80[![CopyLeft](img/copyright-copyleft.jpg)]]

---
# open Source Licenses

### Permissive

- MIT (Example: Node.js)
- Apache-2.0 (Example: Apache Tomcat)

### Weak Copyleft

- GNU Lesser General Public License (LGPL) (Example: VLC)
- Mozilla Public License (MPL) (Example: LibreOffice, Firefox)

### Strong Copyleft

- GNU General Public License v2 (GPL) (Example: OBS studio, Joomla)
- GNU Affero GPL (AGPL) (Example: Mattermost, Nextcloud)

---
# Source Available

"I have a bad feeling about this."

.center.image1[![alt text](img/tarkin.webp)]

---
# Ethical licenses

.center.image100[![alt text](img/jawa.webp)]

---
# License Changes

Some key informations:

1 - License can change

A lot of projects change their licenses Elastic, MongoDB, Hashicorp, but it's not limited to IT like Open Gaming License (i.e.: Dungeon and Dragons)

2 - Contributors community

A license change can have various impact regarding your contributors origin.

- All contributors come from the same company (Kafka)
- All contributors are independent
- In the middle of the river (JDK)
