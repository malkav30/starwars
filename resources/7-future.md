class: center, middle, inverse
<div id="stars"></div>
<div id="stars2"></div>
<div id="stars3"></div>
## A new hope

???

L’avenir des licences, on est la confédération marchande (open source win, exemple ssh, consommateur sans contribuer) PDL

---
class: slashedright
.pull-left[
## 2024: 2nd redis corruption...

### ...and the rise of a new hope

> While being pressured by several Trade Federations, Redis choses to embrace the dark side of the SSPL
>
> A big part of the population migrate to a new planet named of Valkey
>
> It's story soon gains a lot of attraction, and rumors of the return of the Opensource rebellion propagate rapidly
]

.pull-right[.center.image100[![](img/planet2.png)]] 

---
class: slashedright
.pull-left[
## 2024: oracle adopt open-Tofu

### The Trade Federation adopts Tofu

> The Trade Federations decide to hide a new rebel group in the Hashicorp System

- Open-Tofu is a clone of Terraform(Hashicorp).

- Oracle integrate opentofu in its SaaS Service
]

.pull-right[.center.image100[![](img/planet3.png)]] 

---

## Why light side matter

No vendor lock-in
No market prices randomness

The only way to help open source is to contribute and not only consume

???
Les communs numériques, c'est la combinaison de 3 choses :

1 - une ressource
2 - une communauté de personnes
3 - des règles établies 
