class: center, middle , inverse
<div id="stars"></div>

<div id="stars2"></div>

<div id="stars3"></div>

## A TechForum Movie

---
class: center, middle, inverse
<div id="stars"></div>
<div id="stars2"></div>
<div id="stars3"></div>

#### Co-starring

.pull-left[

## $ Philippe Duval

@ Global Architecture - Cloud & Software Architecture

@ Expert

@ Tech Squad Cloud, Infra & DevOps

<i class="nf nf-dev-java"></i> + <i class="nf nf-dev-git_branch"></i> + <i class="nf nf-fa-cloud"></i>

<i class="nf nf-md-email_outline"></i> **philippe.duval@worldline.com**
]

.pull-right[

## # Thomas Langlois

@ Global Architecture - Cloud & Software Architecture

@ Expert

@ Tech Squad Cloud, Infra & DevOps

<i class="nf nf-dev-java"></i> + <i class="nf nf-dev-git_branch"></i> + <i class="nf nf-fa-cloud"></i>

<i class="nf nf-md-email_outline"></i> **thomas.langlois@worldline.com**
]

---
class: starwars, inverse

<iframe width="100%" height="600" src="./starwars.html" scrolling="no" style="border: none;"></iframe>

