class: center, middle, inverse
<div id="stars"></div>
<div id="stars2"></div>
<div id="stars3"></div>
## The fall of the cncf council

---

# cncf


.center.image80[![alt text](img/cncf.png)]

---
# cncf

CNCF as an industrial standard  

Three Levels of maturity:
- Graduated (in 2022):
  - Committers from at least two organizations.
  - Independent security audit.
  - Define project governance and committer process.
  - Receive a supermajority vote from the TOC to move to graduation stage.
- Incubating
- Sandbox


---
# cncf

.center.image80[![alt text](img/cncf2.png)]

---
# Who is this Tie Fighter

At the end of 2023, Cilium (Isovalent) was bought

.center.image80[![alt text](img/CiscoReturn.png)]
