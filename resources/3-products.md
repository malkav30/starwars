class: center, middle, inverse

# The rise of the Empire
<div id="stars"></div>
<div id="stars2"></div>
<div id="stars3"></div>

---
class: slashedleft

.pull-left[

.center.image100[![](img/planet2.png)]

]

.pull-right[

## 2016: The MariaDB system falls

> After years of commercial trade with the Trade Federation, MariaDB is corrupted by the Empire's dreadful Business Source Licence
>
> All contacts with MariaDB are lost

- MariaDB cannot be used in production as is, without a commercial agreement
- Source code remains available
- Code eventually shifts to an opensource license

]

---
class: slashedright

.pull-left[

## 2018: First corruption of Redis

> Menaced by a greed independant merchant planet, Redis adds a Commons Clause to its tradings agreements

- Redis cannot be resold as is under it's Apache 2.0 License
- This does not prevent selling services using Redis, though the line is blurry

]

.pull-right[

.center.image100[![](img/planet3.png)]

] 

---
class: slashedleft

.pull-left[

.center.image100[![](img/planet1.png)]
]

.pull-right[

## 2018: The Raid on MongoDB

> Besieged by the Trade Federation, MongoDB retaliates with the first incarnation of the Server Side Public Licence

- MongoDB cannot be resold as a service without publishing it's source code
- This does not prevent using MongoDB as part of a commercial service

] 

---
class: slashedright

.pull-left[

## 2021-23: The Clone Wars

> Following the Fall of systems MariaDB and MongoDB, numerous systems turn to the Dark Side of either SSPL or BSL
>
> Both Elastic and Hashicorp Galaxies entirely turn to the dark side for all their systems 
>
> Great Turbulence in the Opensource Rebellion is seen

- More and more Opensource projects are locking their model

]

.pull-right[

.center.image100[![](img/clonewars.png)]

] 


---
class: slashedright

.pull-left[
    
## The Senator oracle

> He has played double game, acting alternatively for the Traders Federation and for the Outer Rim
>
> Many suspect that he is in fact one leader of the Empire...

- ORACLE made numerous damagefull license changes to Java and GraalVM throughout the years, with numerous rollbacks.
- ORACLE still champions Opensource models to this day

]

.pull_right[

.image100[![](img/cisco.png)]

]
