class: center, middle, inverse
<div id="stars"></div>
<div id="stars2"></div>
<div id="stars3"></div>
## i felt a great disturbance in the Force

---
# Rebel Detector

.center.image30[![alt text](img/howtochoose.webp)]

---

# Framework assesment

We have guideline about that:

[[Guideline] - Opensource Criterias for library usage within Worldline projects](https://confluence.worldline-solutions.com/x/2A1tMg):
- Qualification and Selection of Open Source Software
- CNCF Maturity Level
- Apache Maturity Model
- CLOMonitor (tool)
- Open Source Foundation Scorecard
---

# Dependencies Licenses risk

Software Bill of Material like SPDX contains licences hierarchy

.center.image80[![SBOM example](img/deptrack.png)]
