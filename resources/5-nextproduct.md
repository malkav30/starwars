class: center, middle, inverse

<div id="stars"></div>
<div id="stars2"></div>
<div id="stars3"></div>

# The Empire's next apprentices 

---
class: slashedright

.pull-right[

.image100[![alt text](<../img/sith lord.png>)]

]

.pull-left[

## Kafka

> Years of war have exhausted the Confluent system. Several planets already fell for the Confluent Community License.
>
> Planet Kafka is about to collapse, at the mercy of the Empire.

- Kafka as a service offers on the Hyperscalers come in direct competition with Confluent offers
- Confluent next logical move: Confluent Community License for Kafka
- Only the Apache Foundation can prevent this move

> It's a pure matter of **community governance**

]

---
class: slashedright

.pull-left[

## 2023: The Fall of Red Hat

> Red Hat, a long time Rebels General, sees it's life at stakes, and chooses to hide in the anonymity of a greater Trade Federation.
>
> Red Hat Linux is now closed source, albeit Red Hat continues to help the Rebellion under cover.
>
> All hope seems lost...

- RHEL is now closed source
- CentOS disappeared

]

.pull-right[

.center.image100[![](img/planet2.png)]

] 
