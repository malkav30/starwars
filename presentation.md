name: title-slide
class: title

# Architalks #7

# Nov. 9<sup>th</sup> 2023

.image[![architalk](img/architalks.jpg)]

---

# ArchiTalks

> Webinar presentation

.pull-left40[
.middle[
.image80[![webinar](img/webinar.jpg)]
]
]

.pull-left30[

#### Webinar format

* ~15 to 25’ Presentation
* ~15’ Q&A

#### Periodicity

* ~every 4 to 6 weeks

#### Leaders

* GA PMO team

]

.pull-right30[

#### Presenters

* GA architects (+guests)

#### Next topics  

* Architecture Review Board
* Global Shared Services
* IAM
* Technology lifecycle : Journey in production
* […]
]

---

class: subtitle

# License Wars

.image[![cloudnative](img/cloudnative.webp)]

---
class: center, middle

.pull-left[

# <i class="nf nf-fa-user fa-fw"></i> Philippe Duval

@ Global Architecture - Cloud & Software Architecture

@ Expert

@ Tech Squad Cloud, Infra & DevOps

<i class="nf nf-dev-java"></i> + <i class="nf nf-dev-git_branch"></i> + <i class="nf nf-fa-cloud"></i>

<i class="nf nf-md-email_outline"></i> **philippe.duval@worldline.com**
]

.pull-right[

# <i class="nf nf-fa-user fa-fw"></i> Thomas Langlois

@ Global Architecture - Cloud & Software Architecture

@ Expert

@ Tech Squad Cloud, Infra & DevOps

<i class="nf nf-dev-java"></i> + <i class="nf nf-dev-git_branch"></i> + <i class="nf nf-fa-cloud"></i>

<i class="nf nf-md-email_outline"></i> **thomas.langlois@worldline.com**
]

---
class: starwars

<iframe width="100%" height="600" src="./starwars.html" scrolling="no" style="border: none;"></iframe>

---
