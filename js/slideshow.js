sourceUrls = [
	  '1-intro.md',
    '2-licencesTypes.md',
	  '3-products.md',
	  '4-cncf.md',
	  '5-nextproduct.md',
	  '6-framework.md',
	  '7-future.md',
    '8-end.md',
        ]

var xmlhttp = new XMLHttpRequest();

var source = ""

for (var i = 0; i < sourceUrls.length; i++) {
  xmlhttp.open('GET', "resources/" + sourceUrls[i], false);
  xmlhttp.send();

  source += xmlhttp.responseText;

// Files shouldn't have --- at the head or foot
// It is added automatically here
  if (i + 1 < sourceUrls.length) {
    source += "\n---\n"
  }
};

var slideshow = remark.create({
  source: source,
  ratio: "16:9",
  highlightLanguage: 'java',
  highlightStyle: 'monokai',
  highlightLines: true,
  highlightSpans: true,
});

