# License Wars

## Agenda

Duration: 35 Min

Parler de l’évolution des grands projets open source et des raisons de leur changement de licence.
-	Narration atour rebellion (opensource) et empire (source based) et les confédération (CSP + nous) 2-3 min PDL
- Description des différentes licences  (BSL, GPLv2, BSD, GPLv3) assimilation avec des factions) 5 min *** BSL ++ TLS
-	Liste des systemes habité par des factions qui ont rejoint un camp par ordre chronologique suite à des attaques 5-10 min PDL
- Cncf impact (il rejoigne l'empire, cilium = cisco returns) (ancienne republique car status graduated n'est pas suffisant) 3 min + photo l'ancienne prez, on a cru à l'ancienne republique TLS
- Liste des systemes menacés dans la bordure exterieur (kafka, ...)  PDL
-	Framework assesment de GA (Detecteur empire ou rebelle) TLS
-	L’avenir des licences, on est la confédération marchande (open source win, exemple ssh, consommateur sans contribuer) PDL
- conclusion: IA (trouver une analogy) (rachat star wars par dysney = IA, star wars avengers) TLS, Devoreur de monde en mickey

Liste:
- mariadb / mysql  
- mongo   
- elastic (AWS)  
- jdk oracle -> openjdk
- redhat centos
- oracle retour arriere
- redhat centos stream
- oracle graalvm (quarkus ?)
- rust -> crab
- hashicorp terraform (open-tf)
- linkerd/cilium
- redis/valkey
- cncf / telegraf (1 truc safe et un autre)
- clomonitor ( language assesment )  



## Practical concerns

* fontawesome : https://www.nerdfonts.com/cheat-sheet
* Remarks.js
* Mail de com
* Timeline
* 18/10 White session
* 7/11 D Day

## Call For Paper

### Description

Once upon a time, in a galaxy not that far away....
A time of troubles and strong disturbances in the Opensource World is coming. The Trade Confederation of Cloud Providers has bee using the Opensource systems for too long, and great concern about their commercial sustainability has pushed more and more of those systems to join the Dark Side of the Source Available License. 
While a lot of Systems have fallen to the Dark Side, such as HashiCorp, MongoDB and Red Hat, there is hope, as a small group of Opensource alternatives are rebelling against the fallacies of the Source Available License, forming a Rebellion led by modern heroes such as Valkey and OpenTofu.
Join us as we apply lessons from the Star Wars universe to the world of open-source licensing, examining the benefits and drawbacks of open-source licensing, the potential for monetization, and the best practices for navigating these complex legal frameworks. We'll help you better understand the landscape of open-source licensing and empower you to make informed decisions when it comes to choosing between open vs closed source licensing. The force is strong with open-source - will you join the rebellion?


### Additional Notes

When Disney bought the Star Wars license, we where doubtful to say the least. They still managed to screw up badly, culminating with the "somehow Palpatine returns" chaos.
When MongoDB pivoted to their Server Side Public License, a source available license model, little did they know that it would trigger an even greater disturbance in the Opensource Force...
We will guide you through this nightmare of a timeline, full of plot twists and deception amongst the back then Opensource project that pivoted to some kind of Source Available License. We will use the Star Wars narrative to point various changes in the Opensource ecosystem, how they led to numerous product forks and fragmentation of the offer, by illustrating the various projects that pivoted to such a license, the forks they provoked, and how the Cloud providers reacted. Using the fiasco of all three graduated services meshes going private, we will see  how recent commercial acquisitions of former Opensource projects threaten regular business and even pushed the CNCF to reconsider their graduation model. Finally, we will see how to protect Worldline business from toxic licensing models, and preserve the spirit of the Opensource Force.

## Release a full index from the fragments

```bash
cat index_fragment1.html > index_full.html
for i in $(ls resources/*.md); do cat $i >> index_full.html; echo -e "\r\n---\r\n" >> index_full.html;done;
cat index_fragment3.html >> index_full.html
```